package com.mmodpoww.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

open class MainActivity : AppCompatActivity() {
    private var btnHello :Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val namet : TextView = findViewById(R.id.name)
        val name : String = namet.text.toString()
        val idd : TextView = findViewById(R.id.stuid)
        val stuid : String = idd.text.toString()
        btnHello = findViewById<Button>(R.id.btnHello)
        btnHello!!.setOnClickListener{
            val intent = Intent(this,HelloActivity::class.java)
            intent.putExtra("Name", name)
            intent.putExtra("ID", stuid)
            startActivity(intent)
        }

    }
}