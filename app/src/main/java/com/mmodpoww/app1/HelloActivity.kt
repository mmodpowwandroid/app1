package com.mmodpoww.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import java.util.jar.Attributes

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val namet = intent.getStringExtra("Name")
        val name: TextView = findViewById<TextView>(R.id.name)
        name.text = namet
        val stuid = intent.getStringExtra("ID")

        Log.d("NameID", namet.toString())
        Log.d("NameID", stuid.toString())
    }

}